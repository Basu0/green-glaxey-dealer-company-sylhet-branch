/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.0.45-community-nt : Database - green_glaxey
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`green_glaxey` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `green_glaxey`;

/*Table structure for table `abbas` */

DROP TABLE IF EXISTS `abbas`;

CREATE TABLE `abbas` (
  `No` varchar(200) NOT NULL,
  `Quentaty` varchar(1000) default NULL,
  `Taka` varchar(500) default NULL,
  `date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Time` varchar(200) default NULL,
  `productName` varchar(500) default NULL,
  PRIMARY KEY  (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `abbas` */

/*Table structure for table `abbas_payment` */

DROP TABLE IF EXISTS `abbas_payment`;

CREATE TABLE `abbas_payment` (
  `no` varchar(200) default NULL,
  `Taka` varchar(500) default NULL,
  `Date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Way` varchar(200) default NULL,
  `Time` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `abbas_payment` */

/*Table structure for table `calculator` */

DROP TABLE IF EXISTS `calculator`;

CREATE TABLE `calculator` (
  `No` int(200) NOT NULL auto_increment,
  `Name` varchar(200) default NULL,
  `Length` varchar(200) default NULL,
  `Weight` varchar(200) default NULL,
  `Pcs` varchar(200) default NULL,
  `Quentaty` varchar(200) default NULL,
  `Note` varchar(100) default NULL,
  KEY `No` (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `calculator` */

/*Table structure for table `calculator2` */

DROP TABLE IF EXISTS `calculator2`;

CREATE TABLE `calculator2` (
  `No` int(200) default NULL,
  `Name` varchar(200) default NULL,
  `Length` varchar(500) default NULL,
  `Weight` varchar(500) default NULL,
  `Pcs` varchar(500) default NULL,
  `Quentaty` varchar(500) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `calculator2` */

/*Table structure for table `company_profile` */

DROP TABLE IF EXISTS `company_profile`;

CREATE TABLE `company_profile` (
  `Id` int(11) NOT NULL auto_increment,
  `Name` varchar(200) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `company_profile` */

insert  into `company_profile`(`Id`,`Name`) values (7,'Green'),(8,'kll');

/*Table structure for table `componyorderby` */

DROP TABLE IF EXISTS `componyorderby`;

CREATE TABLE `componyorderby` (
  `Sid` int(200) default NULL,
  `Name` varchar(500) default NULL,
  `Address` varchar(200) default NULL,
  `Mobile` varchar(200) default NULL,
  `InvoiceDate` varchar(200) default NULL,
  `ComponyName` varchar(200) default NULL,
  `ProductName` varchar(500) default NULL,
  `Type` varchar(200) default NULL,
  `Lenght` varchar(500) default NULL,
  `weight` varchar(500) default NULL,
  `pcs` varchar(200) default NULL,
  `Quentaty` varchar(200) default NULL,
  `MM` varchar(200) default NULL,
  `time` varchar(200) default NULL,
  `Orderdate` date default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `componyorderby` */

/*Table structure for table `deliverytable` */

DROP TABLE IF EXISTS `deliverytable`;

CREATE TABLE `deliverytable` (
  `Sid` int(200) default NULL,
  `Name` varchar(200) default NULL,
  `Number` varchar(200) default NULL,
  `hight` varchar(200) default NULL,
  `weight` varchar(200) default NULL,
  `pcs` varchar(200) default NULL,
  `Quentaty` varchar(200) default NULL,
  `product_Name` varchar(200) default NULL,
  `Total_Taka` varchar(200) default NULL,
  `Discout` varchar(200) default NULL,
  `Paid` varchar(200) default NULL,
  `due` varchar(200) default NULL,
  `Address` varchar(200) default NULL,
  `Date` date default NULL,
  `time` varchar(200) default NULL,
  `Company_name` varchar(500) default NULL,
  `Note` varchar(1000) default NULL,
  `mm` varchar(200) default NULL,
  `Type` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `deliverytable` */

/*Table structure for table `green_greanite` */

DROP TABLE IF EXISTS `green_greanite`;

CREATE TABLE `green_greanite` (
  `No` varchar(200) NOT NULL,
  `Quentaty` varchar(1000) default NULL,
  `Taka` varchar(500) default NULL,
  `date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Time` varchar(200) default NULL,
  `productName` varchar(500) default NULL,
  PRIMARY KEY  (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `green_greanite` */

/*Table structure for table `green_greanite_payment` */

DROP TABLE IF EXISTS `green_greanite_payment`;

CREATE TABLE `green_greanite_payment` (
  `no` varchar(200) default NULL,
  `Taka` varchar(500) default NULL,
  `Date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Way` varchar(200) default NULL,
  `Time` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `green_greanite_payment` */

/*Table structure for table `invoice` */

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `SiD` int(200) NOT NULL,
  `Name` varchar(200) default NULL,
  `Address` varchar(200) default NULL,
  `Mobile` int(200) default NULL,
  `date` varchar(200) default NULL,
  `Time` varchar(200) default NULL,
  `Product_Name` varchar(200) default NULL,
  `Type` varchar(200) default NULL,
  `Lenght` varchar(500) default NULL,
  `Weight` varchar(500) default NULL,
  `pcs1` varchar(500) default NULL,
  `Quentaty` varchar(500) default NULL,
  `Rate` varchar(500) default NULL,
  `taka` varchar(500) default NULL,
  `mm` varchar(500) default NULL,
  `Compony_Name` varchar(500) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invoice` */

/*Table structure for table `invoice_2` */

DROP TABLE IF EXISTS `invoice_2`;

CREATE TABLE `invoice_2` (
  `id` int(200) NOT NULL,
  `Customar_Name` varchar(500) default NULL,
  `TotalTaka` varchar(500) default NULL,
  `DisCount` varchar(500) default NULL,
  `DicForAmmount` varchar(500) default NULL,
  `Paid` varchar(500) default NULL,
  `Due` varchar(500) default NULL,
  `Date` varchar(200) default NULL,
  `Time` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invoice_2` */

/*Table structure for table `product_item` */

DROP TABLE IF EXISTS `product_item`;

CREATE TABLE `product_item` (
  `id` int(200) NOT NULL auto_increment,
  `Iteam_Name` varchar(200) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `product_item` */

insert  into `product_item`(`id`,`Iteam_Name`) values (1,'Marble Name'),(2,'Granite Name'),(6,'Black Glaxicy');

/*Table structure for table `product_table` */

DROP TABLE IF EXISTS `product_table`;

CREATE TABLE `product_table` (
  `Id` int(200) NOT NULL auto_increment,
  `itean` varchar(200) default NULL,
  `Product_Name` varchar(200) default NULL,
  `Taka` varchar(200) default NULL,
  `Compony_Name` varchar(200) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `product_table` */

insert  into `product_table`(`Id`,`itean`,`Product_Name`,`Taka`,`Compony_Name`) values (2,'Marble Name','Aliyana Black','P','Green'),(3,'Marble Name','Athena Red ( Red Beauty )','P','SS'),(4,'Marble Name','Atlas Grey','P','Green'),(5,'Granite Name','Brazil Brown','U','Green'),(6,'Granite Name','Crystal Blue','P','m'),(7,'Granite Name','Gold Pearl','','ps'),(8,'Marble Name','Rosaliya',NULL,'hk'),(10,'Granite Name','marbel','250','Green'),(11,'Granite Name','a6','250','Green');

/*Table structure for table `sequrity` */

DROP TABLE IF EXISTS `sequrity`;

CREATE TABLE `sequrity` (
  `id` int(200) NOT NULL auto_increment,
  `User_Stap` varchar(200) default NULL,
  `password_Stap` varchar(200) default NULL,
  `Componey_Name` varchar(200) NOT NULL,
  `Green_Password` varchar(200) NOT NULL default '',
  `Shoid_Password` varchar(200) NOT NULL,
  `Abbas_Password` varchar(200) NOT NULL,
  `wazib_Password` varbinary(200) NOT NULL,
  PRIMARY KEY  (`id`,`Componey_Name`,`Green_Password`,`Shoid_Password`,`Abbas_Password`,`wazib_Password`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `sequrity` */

insert  into `sequrity`(`id`,`User_Stap`,`password_Stap`,`Componey_Name`,`Green_Password`,`Shoid_Password`,`Abbas_Password`,`wazib_Password`) values (1,'abc','123','green & greanite','123','123','123','123'),(2,NULL,NULL,'Sohid','123','','',''),(3,NULL,NULL,'Abbas','123','','',''),(4,NULL,NULL,'wazib','123','','','');

/*Table structure for table `sohid` */

DROP TABLE IF EXISTS `sohid`;

CREATE TABLE `sohid` (
  `No` varchar(200) NOT NULL,
  `Quentaty` varchar(1000) default NULL,
  `Taka` varchar(500) default NULL,
  `date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Time` varchar(200) default NULL,
  `productName` varchar(500) default NULL,
  PRIMARY KEY  (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sohid` */

/*Table structure for table `sohid_payment` */

DROP TABLE IF EXISTS `sohid_payment`;

CREATE TABLE `sohid_payment` (
  `no` varchar(200) default NULL,
  `Taka` varchar(500) default NULL,
  `Date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Way` varchar(200) default NULL,
  `Time` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sohid_payment` */

/*Table structure for table `wazib` */

DROP TABLE IF EXISTS `wazib`;

CREATE TABLE `wazib` (
  `No` varchar(200) NOT NULL,
  `Quentaty` varchar(1000) default NULL,
  `Taka` varchar(500) default NULL,
  `date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Time` varchar(200) default NULL,
  `productName` varchar(500) default NULL,
  PRIMARY KEY  (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wazib` */

/*Table structure for table `wazib_payment` */

DROP TABLE IF EXISTS `wazib_payment`;

CREATE TABLE `wazib_payment` (
  `no` varchar(200) default NULL,
  `Taka` varchar(500) default NULL,
  `Date` varchar(200) default NULL,
  `Monthdate` varchar(200) default NULL,
  `Way` varchar(200) default NULL,
  `Time` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wazib_payment` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
