/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package green_glaxey;

import com.sun.glass.events.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Basu dev
 */
public class Customar_View extends javax.swing.JFrame {
    Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    public Customar_View() {
        initComponents();
        tableshow();
        OrderBy();
    }
     public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/green_glaxey", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Customar_View.class.getName()).log(Level.SEVERE, null, ex);
        }}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        s_id = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        name = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        address = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        mobile = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        invoicedate = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        Compay = new javax.swing.JComboBox<>();
        jLabel18 = new javax.swing.JLabel();
        searchbt = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        product_name = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        type = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        lenght = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        weight = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        pcs = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        quentaty = new javax.swing.JTextField();
        mm = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel3.setBackground(new java.awt.Color(68, 10, 127));
        jPanel3.setLayout(null);

        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel21);
        jLabel21.setBounds(1070, 10, 0, 40);

        jLabel10.setFont(new java.awt.Font("Rockwell", 1, 40)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 204, 0));
        jLabel10.setText("GREEN  GRANITE  &  MARBLE  LTD .");
        jPanel3.add(jLabel10);
        jLabel10.setBounds(210, 10, 760, 60);

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_Minimize_Window_35px.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel20);
        jLabel20.setBounds(1270, 10, 40, 40);

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_Close_Window_35px_1.png"))); // NOI18N
        jLabel26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel26MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel26);
        jLabel26.setBounds(1320, 10, 40, 40);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Untitled-75.png"))); // NOI18N
        jPanel3.add(jLabel4);
        jLabel4.setBounds(70, 0, 80, 60);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 0, 1360, 80);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jLabel3.setBackground(new java.awt.Color(255, 51, 51));
        jLabel3.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel3.setText("Si.No :");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(150, 10, 40, 40);

        s_id.setBackground(new java.awt.Color(204, 204, 204));
        s_id.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        s_id.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                s_idActionPerformed(evt);
            }
        });
        jPanel1.add(s_id);
        s_id.setBounds(190, 10, 60, 40);

        jLabel5.setBackground(new java.awt.Color(255, 51, 51));
        jLabel5.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel5.setText("Name:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(250, 10, 40, 40);

        name.setBackground(new java.awt.Color(204, 204, 204));
        name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameActionPerformed(evt);
            }
        });
        jPanel1.add(name);
        name.setBounds(290, 10, 250, 40);

        jLabel6.setBackground(new java.awt.Color(255, 51, 51));
        jLabel6.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel6.setText("Address:");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(540, 10, 50, 40);

        address.setBackground(new java.awt.Color(204, 204, 204));
        address.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        address.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addressActionPerformed(evt);
            }
        });
        jPanel1.add(address);
        address.setBounds(590, 10, 220, 40);

        jLabel7.setBackground(new java.awt.Color(255, 51, 51));
        jLabel7.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel7.setText("Mobile:");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(810, 10, 50, 40);

        mobile.setBackground(new java.awt.Color(204, 204, 204));
        mobile.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mobile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mobileActionPerformed(evt);
            }
        });
        jPanel1.add(mobile);
        mobile.setBounds(860, 10, 100, 40);

        jLabel8.setBackground(new java.awt.Color(255, 51, 51));
        jLabel8.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel8.setText("Date :");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(960, 10, 40, 40);

        invoicedate.setBackground(new java.awt.Color(204, 204, 204));
        invoicedate.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        invoicedate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                invoicedateActionPerformed(evt);
            }
        });
        jPanel1.add(invoicedate);
        invoicedate.setBounds(1000, 10, 100, 40);

        jLabel14.setBackground(new java.awt.Color(255, 51, 51));
        jLabel14.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel14.setText("Compony:");
        jPanel1.add(jLabel14);
        jLabel14.setBounds(1100, 10, 60, 40);

        Compay.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Compay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Compony" }));
        jPanel1.add(Compay);
        Compay.setBounds(1160, 10, 190, 40);

        jLabel18.setBackground(new java.awt.Color(255, 51, 51));
        jLabel18.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        jLabel18.setText("Serch:");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(0, 10, 40, 40);

        searchbt.setBackground(new java.awt.Color(204, 204, 204));
        searchbt.setFont(new java.awt.Font("Sitka Subheading", 1, 12)); // NOI18N
        searchbt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchbtActionPerformed(evt);
            }
        });
        searchbt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchbtKeyPressed(evt);
            }
        });
        jPanel1.add(searchbt);
        searchbt.setBounds(40, 10, 110, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 80, 1360, 60);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jLabel9.setBackground(new java.awt.Color(255, 51, 51));
        jLabel9.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel9.setText("Product Name :");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(0, 10, 130, 40);

        product_name.setBackground(new java.awt.Color(204, 204, 204));
        product_name.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        product_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product_nameActionPerformed(evt);
            }
        });
        jPanel2.add(product_name);
        product_name.setBounds(130, 10, 310, 40);

        jLabel11.setBackground(new java.awt.Color(255, 51, 51));
        jLabel11.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel11.setText("Type:");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(440, 10, 50, 40);

        type.setBackground(new java.awt.Color(204, 204, 204));
        type.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        type.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeActionPerformed(evt);
            }
        });
        jPanel2.add(type);
        type.setBounds(490, 10, 100, 40);

        jLabel12.setBackground(new java.awt.Color(255, 51, 51));
        jLabel12.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel12.setText("Lenght:");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(590, 10, 70, 40);

        lenght.setBackground(new java.awt.Color(204, 204, 204));
        lenght.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lenght.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lenghtActionPerformed(evt);
            }
        });
        jPanel2.add(lenght);
        lenght.setBounds(660, 10, 80, 40);

        jLabel13.setBackground(new java.awt.Color(255, 51, 51));
        jLabel13.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel13.setText("Weight:");
        jPanel2.add(jLabel13);
        jLabel13.setBounds(740, 10, 70, 40);

        weight.setBackground(new java.awt.Color(204, 204, 204));
        weight.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        weight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weightActionPerformed(evt);
            }
        });
        jPanel2.add(weight);
        weight.setBounds(810, 10, 80, 40);

        jLabel15.setBackground(new java.awt.Color(255, 51, 51));
        jLabel15.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel15.setText("PCS:");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(890, 10, 40, 40);

        pcs.setBackground(new java.awt.Color(204, 204, 204));
        pcs.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        pcs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pcsActionPerformed(evt);
            }
        });
        jPanel2.add(pcs);
        pcs.setBounds(930, 10, 80, 40);

        jLabel16.setBackground(new java.awt.Color(255, 51, 51));
        jLabel16.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel16.setText("Quentaty:");
        jPanel2.add(jLabel16);
        jLabel16.setBounds(1010, 10, 90, 40);

        quentaty.setBackground(new java.awt.Color(204, 204, 204));
        quentaty.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        quentaty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quentatyActionPerformed(evt);
            }
        });
        jPanel2.add(quentaty);
        quentaty.setBounds(1100, 10, 80, 40);

        mm.setBackground(new java.awt.Color(204, 204, 204));
        mm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        mm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mmActionPerformed(evt);
            }
        });
        jPanel2.add(mm);
        mm.setBounds(1230, 10, 100, 40);

        jLabel17.setBackground(new java.awt.Color(255, 51, 51));
        jLabel17.setFont(new java.awt.Font("Sitka Subheading", 1, 18)); // NOI18N
        jLabel17.setText("MM :");
        jPanel2.add(jLabel17);
        jLabel17.setBounds(1180, 10, 50, 40);

        jButton1.setText("Show");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(0, 80, 100, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 550, 1360, 150);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Name", "Address", "Mobile", "In Date", "P.Name", "Type", "Lenght", "Weight", "pcs", "Quentaty", "Rate", "taka", "mm", "Compony_Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 140, 1360, 410);

        setSize(new java.awt.Dimension(1374, 742));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
            dispose();
        }
    }//GEN-LAST:event_jLabel21MouseClicked

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
        // TODO add your handling code here:
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel26MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel26MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
            dispose();
        }
    }//GEN-LAST:event_jLabel26MouseClicked

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        javax.swing.table.DefaultTableModel ta=(javax.swing.table.DefaultTableModel)jTable1.getModel();
        s_id.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 0).toString());
        name.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 1).toString());
        address.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 2).toString());
        mobile.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 3).toString());
        invoicedate.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 4).toString());
        product_name.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 5).toString());
        type.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 6).toString());
        lenght.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 7).toString());
        weight.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 8).toString());
        pcs.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 9).toString());
        quentaty.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 10).toString());
        mm.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 13).toString());
        Compay.setSelectedItem(ta.getValueAt(jTable1.getSelectedRow(), 14).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void s_idActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_s_idActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_s_idActionPerformed

    private void nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameActionPerformed

    private void addressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addressActionPerformed

    private void mobileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mobileActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mobileActionPerformed

    private void invoicedateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_invoicedateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_invoicedateActionPerformed

    private void product_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product_nameActionPerformed

    private void typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_typeActionPerformed

    private void lenghtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lenghtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lenghtActionPerformed

    private void weightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_weightActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_weightActionPerformed

    private void pcsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pcsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pcsActionPerformed

    private void quentatyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quentatyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quentatyActionPerformed

    private void mmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mmActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mmActionPerformed

    private void searchbtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchbtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchbtActionPerformed

    private void searchbtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchbtKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        try { int a=0,b=0,c=0,d=0,e=0,f=0;
           con();
           javax.swing.table.DefaultTableModel ta =(javax.swing.table.DefaultTableModel)jTable1.getModel();
           while(ta.getRowCount()>0){
        for (int i = 0; i <ta.getRowCount(); i++) {
            ta.removeRow(i);
        }}
           String x=searchbt.getText();
           String m="Select `SiD`,`Name`,`Address`,`Mobile`,`date`,`Product_Name`,`Type`,`Lenght`,`Weight`,`pcs1`,`Quentaty`,`Rate`,`taka`,`mm`,`Compony_Name`FROM `invoice` WHERE `SiD`='"+x+"' or `Name`='"+x+"'or `Address`='"+x+"' or `Mobile`='"+x+"' or `date`='"+x+"'or `Product_Name`='"+x+"'or `Lenght`='"+x+"'";
           st=con.prepareCall(m);
           rs=st.executeQuery();
           while(rs.next()){
               
           ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15)});
           }
       } catch (SQLException ex) {
           Logger.getLogger(CompaneOrder.class.getName()).log(Level.SEVERE, null, ex);
       } 
        }
    }//GEN-LAST:event_searchbtKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        javax.swing.table.DefaultTableModel ta =(javax.swing.table.DefaultTableModel)jTable1.getModel();
           while(ta.getRowCount()>0){
        for (int i = 0; i <ta.getRowCount(); i++) {
            ta.removeRow(i);
        }}
        tableshow();
    }//GEN-LAST:event_jButton1ActionPerformed

    public void tableshow(){
   try { int a=0,b=0,c=0,d=0,e=0,f=0;
           con();
           javax.swing.table.DefaultTableModel ta =(javax.swing.table.DefaultTableModel)jTable1.getModel();
           String m="Select `SiD`,`Name`,`Address`,`Mobile`,`date`,`Product_Name`,`Type`,`Lenght`,`Weight`,`pcs1`,`Quentaty`,`Rate`,`taka`,`mm`,`Compony_Name`FROM `invoice`";
           st=con.prepareCall(m);
           rs=st.executeQuery();
           while(rs.next()){
               
           ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15)});
           }
       } catch (SQLException ex) {
           Logger.getLogger(CompaneOrder.class.getName()).log(Level.SEVERE, null, ex);
       }   
      
      
  }
    public void OrderBy(){
    con();
    //TestCatagory_combo.addItem("Select Test");
    String Q="SELECT `Name` FROM `company_profile`";
        try {
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next()){
            Compay.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(invoice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Customar_View.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Customar_View.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Customar_View.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Customar_View.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Customar_View().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Compay;
    private javax.swing.JTextField address;
    private javax.swing.JTextField invoicedate;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField lenght;
    private javax.swing.JTextField mm;
    private javax.swing.JTextField mobile;
    private javax.swing.JTextField name;
    private javax.swing.JTextField pcs;
    private javax.swing.JTextField product_name;
    private javax.swing.JTextField quentaty;
    private javax.swing.JTextField s_id;
    private javax.swing.JTextField searchbt;
    private javax.swing.JTextField type;
    private javax.swing.JTextField weight;
    // End of variables declaration//GEN-END:variables
}
