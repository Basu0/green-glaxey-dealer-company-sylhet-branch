/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package green_glaxey;

import java.awt.Color;
import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Basu dev
 */
public class DeliveryShow extends javax.swing.JFrame {

    Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    public DeliveryShow() {
        initComponents();
        tableshow();
    }

    public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/green_glaxey", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(invoice.class.getName()).log(Level.SEVERE, null, ex);
        }}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        customarName = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        S_no = new javax.swing.JTextField();
        customarName1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        customarName4 = new javax.swing.JTextField();
        customarName3 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        customarName6 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        customarName7 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        customarName8 = new javax.swing.JTextField();
        customarName9 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        customarName10 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        customarName12 = new javax.swing.JTextField();
        customarName11 = new javax.swing.JTextField();
        S_no1 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        customarName13 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        customarName14 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        S_no2 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        Background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel4.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel4.setText("Customer  Name :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 110, 110, 40);

        customarName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarNameActionPerformed(evt);
            }
        });
        getContentPane().add(customarName);
        customarName.setBounds(110, 110, 230, 40);

        jLabel17.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel17.setText("Date :");
        getContentPane().add(jLabel17);
        jLabel17.setBounds(180, 60, 40, 40);

        S_no.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        S_no.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                S_noActionPerformed(evt);
            }
        });
        getContentPane().add(S_no);
        S_no.setBounds(220, 60, 120, 40);

        customarName1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName1ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName1);
        customarName1.setBounds(110, 160, 120, 40);

        jLabel5.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel5.setText("Number                  :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 160, 110, 40);

        jLabel7.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel7.setText("Lenght                    :");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(0, 210, 110, 40);

        jLabel8.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel8.setText("Weight :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(200, 210, 50, 40);

        customarName4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName4ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName4);
        customarName4.setBounds(250, 210, 90, 40);

        customarName3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName3ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName3);
        customarName3.setBounds(110, 210, 90, 40);

        jLabel9.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel9.setText("PCS                           :");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(0, 260, 110, 40);

        jLabel11.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel11.setText("Quentaty :");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(190, 260, 70, 40);

        customarName6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName6ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName6);
        customarName6.setBounds(250, 260, 90, 40);

        jLabel12.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel12.setText("Product Name      :");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(0, 310, 110, 40);

        customarName7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName7ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName7);
        customarName7.setBounds(110, 310, 230, 40);

        jLabel13.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel13.setText("Total Taka            :");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(0, 360, 110, 40);

        customarName8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName8ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName8);
        customarName8.setBounds(110, 360, 80, 40);

        customarName9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName9ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName9);
        customarName9.setBounds(250, 360, 90, 40);

        jLabel14.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel14.setText("Discount :");
        getContentPane().add(jLabel14);
        jLabel14.setBounds(190, 360, 60, 40);

        jLabel15.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel15.setText("Paid                        :");
        getContentPane().add(jLabel15);
        jLabel15.setBounds(0, 410, 110, 40);

        customarName10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName10ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName10);
        customarName10.setBounds(110, 260, 80, 40);

        jLabel16.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel16.setText("Due          :");
        getContentPane().add(jLabel16);
        jLabel16.setBounds(190, 410, 70, 40);

        customarName12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName12ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName12);
        customarName12.setBounds(110, 410, 80, 40);

        customarName11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName11ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName11);
        customarName11.setBounds(250, 410, 90, 40);

        S_no1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        S_no1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                S_no1ActionPerformed(evt);
            }
        });
        getContentPane().add(S_no1);
        S_no1.setBounds(110, 60, 70, 40);

        jLabel19.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel19.setText("Delivery Address :");
        getContentPane().add(jLabel19);
        jLabel19.setBounds(0, 460, 110, 40);

        customarName13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName13ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName13);
        customarName13.setBounds(110, 460, 230, 40);

        jLabel18.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel18.setText("Invoice No :");
        getContentPane().add(jLabel18);
        jLabel18.setBounds(10, 60, 70, 40);

        jLabel20.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel20.setText("Companey Name :");
        getContentPane().add(jLabel20);
        jLabel20.setBounds(0, 510, 110, 40);

        customarName14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        customarName14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customarName14ActionPerformed(evt);
            }
        });
        getContentPane().add(customarName14);
        customarName14.setBounds(110, 510, 230, 40);

        jLabel21.setFont(new java.awt.Font("Sitka Display", 1, 14)); // NOI18N
        jLabel21.setText("Time :");
        getContentPane().add(jLabel21);
        jLabel21.setBounds(230, 160, 40, 40);

        S_no2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        S_no2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                S_no2ActionPerformed(evt);
            }
        });
        getContentPane().add(S_no2);
        S_no2.setBounds(270, 160, 70, 40);

        jButton1.setText("Submit");
        getContentPane().add(jButton1);
        jButton1.setBounds(10, 580, 80, 40);

        jPanel1.setBackground(new java.awt.Color(0, 128, 128));

        jLabel10.setFont(new java.awt.Font("Rockwell", 1, 40)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 204, 0));
        jLabel10.setText("GREEN  GRANITE  &  MARBLE  LTD .");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(170, Short.MAX_VALUE)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 756, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(104, 104, 104))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(370, 0, 1030, 80);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "I.No", "Customer Name", "Number", "Lenght", "Weight", "Pcs", "Quentaty", "P.Name", "Total Taka", "Discount", "Paid", "Due", "D.Address", "Date", "Time", "Companey"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(370, 80, 990, 630);

        Background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/2632323.png"))); // NOI18N
        Background.setText("jLabel1");
        getContentPane().add(Background);
        Background.setBounds(0, 0, 370, 710);

        setSize(new java.awt.Dimension(1371, 748));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void customarNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarNameActionPerformed

    private void S_noActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_S_noActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_S_noActionPerformed

    private void customarName1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName1ActionPerformed

    private void customarName3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName3ActionPerformed

    private void customarName4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName4ActionPerformed

    private void customarName6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName6ActionPerformed

    private void customarName7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName7ActionPerformed

    private void customarName8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName8ActionPerformed

    private void customarName9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName9ActionPerformed

    private void customarName10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName10ActionPerformed

    private void customarName11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName11ActionPerformed

    private void customarName12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName12ActionPerformed

    private void S_no1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_S_no1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_S_no1ActionPerformed

    private void customarName13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName13ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName13ActionPerformed

    private void customarName14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customarName14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customarName14ActionPerformed

    private void S_no2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_S_no2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_S_no2ActionPerformed
   public void tableshow(){
   try { int a=0,b=0,c=0,d=0,e=0,f=0;
           con();
           javax.swing.table.DefaultTableModel ta =(javax.swing.table.DefaultTableModel)jTable1.getModel();
           String m="Select `Sid`,`Name`,`Number`,`hight`,`weight`,`pcs`,`Quentaty`,`product_Name`,`Total_Taka`,`Discout`,`Paid`,`due`,`Address`,`Date`,`time`,`Company_name`FROM `DeliveryTable`";
           st=con.prepareCall(m);
           rs=st.executeQuery();
           while(rs.next()){
               
           ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),rs.getString(16)});
           }
       } catch (SQLException ex) {
           Logger.getLogger(CompaneOrder.class.getName()).log(Level.SEVERE, null, ex);
       }}
   
   public void color(){
       
      
   }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DeliveryShow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DeliveryShow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DeliveryShow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DeliveryShow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DeliveryShow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Background;
    private javax.swing.JTextField S_no;
    private javax.swing.JTextField S_no1;
    private javax.swing.JTextField S_no2;
    private javax.swing.JTextField customarName;
    private javax.swing.JTextField customarName1;
    private javax.swing.JTextField customarName10;
    private javax.swing.JTextField customarName11;
    private javax.swing.JTextField customarName12;
    private javax.swing.JTextField customarName13;
    private javax.swing.JTextField customarName14;
    private javax.swing.JTextField customarName3;
    private javax.swing.JTextField customarName4;
    private javax.swing.JTextField customarName6;
    private javax.swing.JTextField customarName7;
    private javax.swing.JTextField customarName8;
    private javax.swing.JTextField customarName9;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
